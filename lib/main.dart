import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'registration.dart';
import 'login.dart';
import 'dashboard.dart';

import 'package:firebase_core/firebase_core.dart';
import 'firebase_options.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'dart:async';

void main() async {
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter App',
      theme: ThemeData(
        scaffoldBackgroundColor: Colors.white,
        primarySwatch: Colors.yellow,
        fontFamily: 'OpenSans',
      ),
      home: SplashScreen(
        seconds: 7,
        navigateAfterSeconds:
            const RegistrationPage(title: 'Registration Page'),
        title: const Text(
          'SplashScreen',
          style: TextStyle(
              fontWeight: FontWeight.bold, fontSize: 20.0, color: Colors.black),
        ),
        backgroundColor: Colors.yellow,
      ),
    );
  }
}
