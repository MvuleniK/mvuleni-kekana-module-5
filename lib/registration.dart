import 'package:flutter/material.dart';

import 'login.dart';

import 'package:firebase_auth/firebase_auth.dart';

import 'dart:async';

class RegistrationPage extends StatefulWidget {
  const RegistrationPage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<RegistrationPage> createState() => _RegistrationPageState();
}

class _RegistrationPageState extends State<RegistrationPage> {
  final usernameInput = TextEditingController();
  final emailInput = TextEditingController();
  final passwordInput = TextEditingController();

  bool valid = true;

  Future<void> _authenticate() async {
    valid = true;

    try {
      final credential =
          await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: emailInput.text,
        password: passwordInput.text,
      );
    } on FirebaseAuthException catch (e) {
      valid = false;
    }

    if (valid) {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => const LoginPage(title: 'Login Page')));
    }
  }

  void _changePage() {
    setState(() {
      _authenticate;
    });
  }

  void dispose() {
    usernameInput.dispose();
    emailInput.dispose();
    passwordInput.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'Username',
            ),
            SizedBox(
              width: 600,
              child: TextField(
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'Enter your user name here',
                  labelText: 'User Name',
                ),
                controller: usernameInput,
              ),
            ),
            const Text(
              'Email Address',
            ),
            SizedBox(
              width: 600,
              child: TextField(
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'Enter your email address here',
                  labelText: 'Email Address',
                ),
                controller: emailInput,
              ),
            ),
            const Text(
              'Password',
            ),
            SizedBox(
              width: 600,
              child: TextField(
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'Enter you password here',
                  labelText: 'Password',
                ),
                obscureText: true,
                controller: passwordInput,
              ),
            ),
            const SizedBox(height: 30),
            ClipRRect(
                borderRadius: BorderRadius.circular(6),
                child: Stack(children: <Widget>[
                  Positioned.fill(
                    child: Container(
                      decoration: const BoxDecoration(color: Colors.yellow),
                    ),
                  ),
                  Tooltip(
                    message: 'Register',
                    child: TextButton(
                      style: TextButton.styleFrom(
                        padding: const EdgeInsets.all(16.0),
                        primary: Colors.white,
                        textStyle: const TextStyle(fontSize: 20),
                      ),
                      onPressed: _changePage,
                      child: const Text('Register'),
                    ),
                  ),
                ]))
          ],
        ),
      ),
    );
  }
}
