import 'package:flutter/material.dart';
import 'dashboard.dart';

import 'package:firebase_auth/firebase_auth.dart';

import 'dart:async';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final emailInput = TextEditingController();
  final passwordInput = TextEditingController();

  bool valid = true;

  Future<void> _signIn() async {
    valid = true;

    try {
      final credential = await FirebaseAuth.instance.signInWithEmailAndPassword(
          email: emailInput.text, password: passwordInput.text);

      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => const DashboardPage(title: 'Dashboard')));
    } on FirebaseAuthException catch (e) {
      valid = false;
    }

    // if (valid) {
    //   Navigator.push(
    //       context,
    //       MaterialPageRoute(
    //           builder: (context) => const Dashboard(title: 'Dashboard')));
    // }
  }

  void _changePage() {
    setState(() {
      _signIn;
    });
  }

  void dispose() {
    emailInput.dispose();
    passwordInput.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'Email',
            ),
            SizedBox(
              width: 600,
              child: TextField(
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'Enter your email here',
                  labelText: 'Email',
                ),
                controller: emailInput,
              ),
            ),
            const Text(
              'Password',
            ),
            SizedBox(
              width: 600,
              child: TextField(
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'Enter your account password here',
                  labelText: 'Password',
                ),
                obscureText: true,
                controller: passwordInput,
              ),
            ),
            const SizedBox(height: 30),
            ClipRRect(
                borderRadius: BorderRadius.circular(4),
                child: Stack(children: <Widget>[
                  Positioned.fill(
                    child: Container(
                      decoration: const BoxDecoration(
                        color: Colors.yellow,
                      ),
                    ),
                  ),
                  TextButton(
                    style: TextButton.styleFrom(
                      padding: const EdgeInsets.all(16.0),
                      primary: Colors.white,
                      textStyle: const TextStyle(fontSize: 20),
                    ),
                    onPressed: _changePage,
                    child: const Text('Login'),
                  ),
                ]))
          ],
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
